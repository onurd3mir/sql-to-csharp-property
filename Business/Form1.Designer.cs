﻿
namespace Business
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.create = new System.Windows.Forms.Button();
            this.serverNameLabel = new System.Windows.Forms.Label();
            this.serverText = new System.Windows.Forms.TextBox();
            this.dbNameLabel = new System.Windows.Forms.Label();
            this.dbText = new System.Windows.Forms.TextBox();
            this.userIdLabel = new System.Windows.Forms.Label();
            this.userIdText = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.nameSpaceLabel = new System.Windows.Forms.Label();
            this.nameSpaceText = new System.Windows.Forms.TextBox();
            this.implementsLabel = new System.Windows.Forms.Label();
            this.implementsText = new System.Windows.Forms.TextBox();
            this.ImplementsPathLabel = new System.Windows.Forms.Label();
            this.implementsPathText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // create
            // 
            this.create.Location = new System.Drawing.Point(162, 269);
            this.create.Name = "create";
            this.create.Size = new System.Drawing.Size(231, 34);
            this.create.TabIndex = 0;
            this.create.Text = "CREATE";
            this.create.UseVisualStyleBackColor = true;
            this.create.Click += new System.EventHandler(this.create_Click_1);
            // 
            // serverNameLabel
            // 
            this.serverNameLabel.AutoSize = true;
            this.serverNameLabel.Location = new System.Drawing.Point(32, 30);
            this.serverNameLabel.Name = "serverNameLabel";
            this.serverNameLabel.Size = new System.Drawing.Size(57, 20);
            this.serverNameLabel.TabIndex = 1;
            this.serverNameLabel.Text = "Server :";
            // 
            // serverText
            // 
            this.serverText.Location = new System.Drawing.Point(162, 27);
            this.serverText.Name = "serverText";
            this.serverText.Size = new System.Drawing.Size(231, 26);
            this.serverText.TabIndex = 2;
            // 
            // dbNameLabel
            // 
            this.dbNameLabel.AutoSize = true;
            this.dbNameLabel.Location = new System.Drawing.Point(32, 62);
            this.dbNameLabel.Name = "dbNameLabel";
            this.dbNameLabel.Size = new System.Drawing.Size(80, 20);
            this.dbNameLabel.TabIndex = 3;
            this.dbNameLabel.Text = "DB Name :";
            // 
            // dbText
            // 
            this.dbText.Location = new System.Drawing.Point(162, 59);
            this.dbText.Name = "dbText";
            this.dbText.Size = new System.Drawing.Size(231, 26);
            this.dbText.TabIndex = 4;
            // 
            // userIdLabel
            // 
            this.userIdLabel.AutoSize = true;
            this.userIdLabel.Location = new System.Drawing.Point(32, 94);
            this.userIdLabel.Name = "userIdLabel";
            this.userIdLabel.Size = new System.Drawing.Size(62, 20);
            this.userIdLabel.TabIndex = 5;
            this.userIdLabel.Text = "User Id :";
            // 
            // userIdText
            // 
            this.userIdText.Location = new System.Drawing.Point(162, 91);
            this.userIdText.Name = "userIdText";
            this.userIdText.Size = new System.Drawing.Size(231, 26);
            this.userIdText.TabIndex = 6;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(32, 126);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(77, 20);
            this.passwordLabel.TabIndex = 7;
            this.passwordLabel.Text = "Password :";
            // 
            // passwordText
            // 
            this.passwordText.Location = new System.Drawing.Point(162, 123);
            this.passwordText.Name = "passwordText";
            this.passwordText.Size = new System.Drawing.Size(231, 26);
            this.passwordText.TabIndex = 8;
            // 
            // nameSpaceLabel
            // 
            this.nameSpaceLabel.AutoSize = true;
            this.nameSpaceLabel.Location = new System.Drawing.Point(32, 158);
            this.nameSpaceLabel.Name = "nameSpaceLabel";
            this.nameSpaceLabel.Size = new System.Drawing.Size(94, 20);
            this.nameSpaceLabel.TabIndex = 9;
            this.nameSpaceLabel.Text = "Namespace :";
            // 
            // nameSpaceText
            // 
            this.nameSpaceText.Location = new System.Drawing.Point(162, 155);
            this.nameSpaceText.Name = "nameSpaceText";
            this.nameSpaceText.Size = new System.Drawing.Size(231, 26);
            this.nameSpaceText.TabIndex = 10;
            // 
            // implementsLabel
            // 
            this.implementsLabel.AutoSize = true;
            this.implementsLabel.Location = new System.Drawing.Point(32, 190);
            this.implementsLabel.Name = "implementsLabel";
            this.implementsLabel.Size = new System.Drawing.Size(94, 20);
            this.implementsLabel.TabIndex = 11;
            this.implementsLabel.Text = "Implements :";
            // 
            // implementsText
            // 
            this.implementsText.Location = new System.Drawing.Point(162, 187);
            this.implementsText.Name = "implementsText";
            this.implementsText.Size = new System.Drawing.Size(231, 26);
            this.implementsText.TabIndex = 12;
            // 
            // ImplementsPathLabel
            // 
            this.ImplementsPathLabel.AutoSize = true;
            this.ImplementsPathLabel.Location = new System.Drawing.Point(32, 223);
            this.ImplementsPathLabel.Name = "ImplementsPathLabel";
            this.ImplementsPathLabel.Size = new System.Drawing.Size(126, 20);
            this.ImplementsPathLabel.TabIndex = 13;
            this.ImplementsPathLabel.Text = "Implements Path :";
            // 
            // implementsPathText
            // 
            this.implementsPathText.Location = new System.Drawing.Point(162, 220);
            this.implementsPathText.Name = "implementsPathText";
            this.implementsPathText.Size = new System.Drawing.Size(231, 26);
            this.implementsPathText.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 339);
            this.Controls.Add(this.implementsPathText);
            this.Controls.Add(this.ImplementsPathLabel);
            this.Controls.Add(this.implementsText);
            this.Controls.Add(this.implementsLabel);
            this.Controls.Add(this.nameSpaceText);
            this.Controls.Add(this.nameSpaceLabel);
            this.Controls.Add(this.passwordText);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.userIdText);
            this.Controls.Add(this.userIdLabel);
            this.Controls.Add(this.dbText);
            this.Controls.Add(this.dbNameLabel);
            this.Controls.Add(this.serverText);
            this.Controls.Add(this.serverNameLabel);
            this.Controls.Add(this.create);
            this.Name = "Form1";
            this.Text = "SQL TO CSHARP PROPERTY";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button create;
        private System.Windows.Forms.Label serverNameLabel;
        private System.Windows.Forms.TextBox serverText;
        private System.Windows.Forms.Label dbNameLabel;
        private System.Windows.Forms.TextBox dbText;
        private System.Windows.Forms.Label userIdLabel;
        private System.Windows.Forms.TextBox userIdText;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.Label nameSpaceLabel;
        private System.Windows.Forms.TextBox nameSpaceText;
        private System.Windows.Forms.Label implementsLabel;
        private System.Windows.Forms.TextBox implementsText;
        private System.Windows.Forms.Label ImplementsPathLabel;
        private System.Windows.Forms.TextBox implementsPathText;
    }
}

