﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void create_Click_1(object sender, EventArgs e)
        {
            string serverName, dbName, userId, password, connectionText,
                nameSpace, implements, implementsPath;
            serverName = serverText.Text;

            dbName = dbText.Text;
            userId = userIdText.Text;
            password = passwordText.Text;
            nameSpace = nameSpaceText.Text;
            implements = implementsText.Text;
            implementsPath = implementsPathText.Text;

            var tableList = new List<string>();

            connectionText = $"Server={serverName};Database={dbName};";
            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(password))
            {
                connectionText += $"User Id={userId};Password={password};";
            }

            connectionText += "Integrated Security=true;";

            //string dir = Application.StartupPath.ToString() + dbName;
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\" + dbName;

            if (!Directory.Exists(dir))
            {
                //MessageBox.Show(dir);
                Directory.CreateDirectory(dir);
            }


            using (var sqlconnection = new SqlConnection(connectionText))
            {

                sqlconnection.Open();

                string tableListQuery = "SELECT * FROM INFORMATION_SCHEMA.TABLES";

                var sqlCommad = new SqlCommand(tableListQuery, sqlconnection);
                var dr = sqlCommad.ExecuteReader();

                while (dr.Read())
                {
                    string tableName = dr["TABLE_NAME"].ToString();
                    tableList.Add(tableName);
                }
                dr.Close();

                string tableColumQuery = "";
                string propertyText = "";


                tableList.ForEach(t =>
                {
                    string newFile = "using System;\nusing System.Collections.Generic;\n";
                    newFile += "using System.Linq;\nusing System.Text;\n";

                    if (!string.IsNullOrEmpty(implementsPath))
                    {
                        newFile += implementsPath + "\n";
                    }

                    newFile += "\nnamespace " + nameSpace + "\n{\n";

                    string tableName = "";
                    if (t.Contains("ies"))
                    {
                        tableName = t.Contains("ies") ? t.Substring(0, t.Length - 3) + "y" : t;
                    }
                    else
                    {
                        tableName = t[t.Length - 1] == 's' ? t.Substring(0, t.Length - 1) : t;
                    }

                    newFile += "\tpublic class " + tableName; 

                    if (!string.IsNullOrEmpty(implements))
                    {
                        newFile += ":"+implements;
                    }

                    newFile+="\n\t{\n";

                    tableColumQuery = $"SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '{t}' ";

                    var sqlcommad = new SqlCommand(tableColumQuery, sqlconnection);
                    var dr = sqlcommad.ExecuteReader();

                    while (dr.Read())
                    {
                        propertyText = "\t\tpublic " + sqlToCsharp(dr["DATA_TYPE"].ToString()) + " " + dr["COLUMN_NAME"].ToString() + " { get; set; }\n";
                        newFile += propertyText;
                    }
                    dr.Close();

                    newFile += "\t}\n}";

                    //string path = @"c:\temp\MyTest.txt";
                    string path = dir + "\\" + tableName + ".cs";

                    try
                    {
                        // Create the file, or overwrite if the file exists.
                        using (FileStream fs = File.Create(path))
                        {
                            byte[] info = new UTF8Encoding(true).GetBytes(newFile);
                            // Add some information to the file.
                            fs.Write(info, 0, info.Length);
                        }

                        // Open the stream and read it back.
                        //using (StreamReader sr = File.OpenText(path))
                        //{
                        //    string s = "";
                        //    while ((s = sr.ReadLine()) != null)
                        //    {
                        //        Console.WriteLine(s);
                        //    }
                        //}
                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                    //MessageBox.Show(newFile);
                });

                sqlconnection.Close();

                MessageBox.Show("Dosyalar Masaüstüne oluşturuldu");

            }

        }


        public string sqlToCsharp(string value)
        {
            return value switch
            {
                "bigint" => "long",
                "bit" => "bool",
                "varchar" => "string",
                "binary" => "byte[]",
                "char" => "string",
                "date" => "DateTime",
                "datetime" => "DateTime",
                "datetime2" => "DateTime",
                "datetimeoffset" => "DateTimeOffset",
                "decimal" => "decimal",
                "filestream" => "byte[]",
                "float" => "double",
                "geography" => "Microsoft.SqlServer.Types.SqlGeography",
                "geometry" => "Microsoft.SqlServer.Types.SqlGeometry",
                "hierarchyid" => "Microsoft.SqlServer.Types.SqlHierarchyid",
                "image" => "byte[]",
                "int" => "int",
                "money" => "decimal",
                "nchar" => "string",
                "ntext" => "string",
                "numeric" => "decimal",
                "nvarchar" => "string",
                "real" => "Single",
                "rowversion" => "byte[]",
                "smalldatetime" => "DateTime",
                "smallint" => "short",
                "smallmoney" => "decimal",
                "sql_variant" => "object",
                "text" => "string",
                "time" => "TimeSpan",
                "timestamp" => "byte[]",
                "tinyint" => "byte",
                "uniqueidentifier" => "Guid",
                "varbinary" => "byte[]",
                "xml" => "string",
                _ => throw new Exception("not found property"),
            };
        }


    }
}
